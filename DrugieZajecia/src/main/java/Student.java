public class Student {

    private String name;
    private String lastname;
    private int index;
    private status status;
    private static int numberOfStudents;
    private int studentNumber;

    public void printAll() {

        System.out.println("name: " + this.name
                + " lastname: " + this.lastname
                + " index: " + this.index
                + " status " + this.status);

    }

    public Student(String name, String lastname, int index, Student.status status) {
        this.name = name;
        this.lastname = lastname;
        this.index = index;
        this.status = status;
        this.studentNumber++;
        this.numberOfStudents++;

        //System.out.println("sn:" + studentNumber + " nos:" +numberOfStudents);
    }

    public Student(String name, String lastname, int index) {
        this.name = name;
        this.lastname = lastname;
        this.index = index;
        this.status = status.REKRUT;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Student.status getStatus() {
        return status;
    }

    public void setStatus(Student.status status) {
        this.status = status;
    }

    public enum status {

        REKRUT, AKTYWNY, WARUNKOWY, CZASOWY

    }

}
